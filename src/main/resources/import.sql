--TYP SAMOCHODU--
INSERT INTO car_type(is_active, name) values(true, 'Sedan');
INSERT INTO car_type(is_active, name) values(true, 'Hatchback');
INSERT INTO car_type(is_active, name) values(true, 'Coupe');

--TYP UMOWY--
INSERT INTO agreement_type(is_active, name) values(true, 'sales');
INSERT INTO agreement_type(is_active, name) values(true, 'purchase');
INSERT INTO agreement_type(is_active, name) values(true, 'cession');

--TYP ROLI--
INSERT INTO role_type(is_active, name) values(true, 'ADMIN');
INSERT INTO role_type(is_active, name) values(true, 'USER');
INSERT INTO role_type(is_active, name) values(true, 'EMPLOYEE');
INSERT INTO role_type(is_active, name) values(true, 'MECHANIC');

--CARS--
INSERT INTO car(added, description, distance, engine, fuel_type, gear_box, horse_power, liability_number, mark, model, registartion_number, vin, year, car_type_id, person_id) VALUES (date(now()), 'ALFA ROMEO GIULIETTA HATCHBACK SPECIAL ED 1.6 JTDM-2 Collezione 5dr hatchback special edition', 190000,'1,9TDi', 'Diesel', 'Manuala', 120, '1235YYYYRRRR6666', 'Alfa Romeo', 'GIULIETTA', 'ZTZ 879860','23436436747654OOOZZZZ', '2007', 1 , null);
INSERT INTO car(added, description, distance, engine, fuel_type, gear_box, horse_power, liability_number, mark, model, registartion_number, vin, year, car_type_id, person_id) VALUES (date(now()), 'AUDI A4 DIESEL AVANT 2.0 TDI 150 S Line 5dr Multitronic [Nav]', 210000,'2,0T', 'Benzyna', 'Manualna', 220, '1235YYYYRRRR4444', 'Audi', 'A4', 'CTG 779450','345345345747654OOOZZZZ', '2012', 2 , null);
INSERT INTO car(added, description, distance, engine, fuel_type, gear_box, horse_power, liability_number, mark, model, registartion_number, vin, year, car_type_id, person_id) VALUES (date(now()), 'PEUGEOT 208 1.2 PureTech Active 5dr hatchback', 210000,'2,0T', 'Petrol', 'Manualna', 220, '1235YYYYRRRR4444', 'Peugot', '208', 'CTG 779450','345345345747654OOOZZZZ', '2016', 2 , null);

--DODANIE OSOBY--
INSERT INTO person(type,added, adress, name, nip, pesel, surname, hire_date, user_id) VALUES ('employee',date(now()), 'Katowicka 48b', 'Dawid', 'ZZ4324235', '3463456345734754357', 'Kwiatkowski',date(now()), null);
INSERT INTO person(type,added, adress, name, nip, pesel, surname, hire_date, user_id) VALUES ('person',date(now()), 'Piotrowska 48b', 'Patryk', 'GG4SDFSDF5', '345646464564357', 'Niewiadomski',date(now()), null);

--USERS--
INSERT INTO users(added, password, username, id_person, email) values(date(now()),'admin','admin', null,'a@a');
INSERT INTO users(added, password, username, id_person, email) values(date(now()),'customer','customer', null,'a@a');

--USERS ROLE JOINED TABLES--
INSERT INTO user_role(user_id, role_id) values(1, 1);
INSERT INTO user_role(user_id, role_id) values(2, 3);
INSERT INTO user_role(user_id, role_id) values(2, 4);

--AGREEMENT--
INSERT INTO agreement(added, amount, description, agreement_type_id, car_id, person_id) VALUES (date(now()), 123900, 'aaaaaaaaaaaa',3,1,1);
INSERT INTO agreement(added, amount, description, agreement_type_id, car_id, person_id) VALUES (date(now()), 153900, 'zzzzzzzzz',2,2,2);
INSERT INTO agreement(added, amount, description, agreement_type_id, car_id, person_id) VALUES (date(now()), 153900, 'bbbbb',1,3,2);

--UPDATES CARS--
UPDATE car SET person_id =1 WHERE id = 1;
UPDATE car SET person_id =1 WHERE id = 2;
UPDATE car SET person_id =1 WHERE id = 3;

--UPDATES PERSON USERS--
UPDATE person SET user_id =1 WHERE id = 1;
UPDATE person SET user_id =2 WHERE id = 2;

--UPDATES USERS--
UPDATE users SET id_person =1 WHERE id=1;
UPDATE users SET id_person =2 WHERE id=2;
