package pl.autokomis.Service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.autokomis.Dto.PersonDto;
import pl.autokomis.Entity.Person;
import pl.autokomis.Entity.User;
import pl.autokomis.Repository.PersonRepository;
import pl.autokomis.Repository.UserRepository;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private UserRepository userRepository;



    public void savePersonInfo(PersonDto personDto, Integer userId){
        Person person = getPerson(personDto);
        User user = userRepository.findOne(userId);
        person.setUser(user);
        personRepository.save(person);
        user.setPerson(person);
        userRepository.save(user);
    }


    public void savePersonDetails(PersonDto personDto, String username){
        User user = userRepository.findByUsername(username);
        Person person = getPerson(personDto);
        if(user.getPerson() == null){
            person.setUser(user);
            personRepository.save(person);
            user.setPerson(person);
            userRepository.save(user);
        }else{
            Person personUpdate = personRepository.findByUserId(user.getId());
            personUpdate.setName(personDto.getName());
            personUpdate.setSurname(personDto.getSurname());
            personUpdate.setAdress(personDto.getAdress());
            personUpdate.setNip(personDto.getNip());
            personUpdate.setPesel(personDto.getPesel());
            personUpdate.setUser(user);
            personRepository.save(personUpdate);
        }
    }



    public PersonDto getPersonDetails(Integer id){

        Person person = personRepository.findByUserId(id);
        if(person == null)
            return null;
        ModelMapper modelMapper = new ModelMapper();
        PersonDto personDto = modelMapper.map(person, PersonDto.class);
        return personDto;
    }


    private Person getPerson(PersonDto personDto){
        ModelMapper modelMapper = new ModelMapper();
        Person person = modelMapper.map(personDto, Person.class);
        return person;
    }




}




