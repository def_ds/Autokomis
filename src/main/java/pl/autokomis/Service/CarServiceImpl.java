package pl.autokomis.Service;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.autokomis.Dto.CarTypeDto;
import pl.autokomis.Dto.NewCarDto;
import pl.autokomis.Entity.Car;
import pl.autokomis.Entity.CarType;
import pl.autokomis.Entity.Person;
import pl.autokomis.Repository.CarRepository;
import pl.autokomis.Repository.CarTypeRepository;
import pl.autokomis.Repository.PersonRepository;

import java.security.acl.Owner;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    private Logger log = LoggerFactory.getLogger(CarServiceImpl.class);

    @Autowired
    private CarTypeRepository carTypeRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private PersonRepository personRepository;


    public List<CarTypeDto> getCarTypeList(){
        List<CarType> carTypes = carTypeRepository.findAll();
        List<CarTypeDto> carTypeDtos = carTypes.stream().map(p-> getCarType(p)).collect(Collectors.toList());
        return carTypeDtos;
    }


    public Car saveCar(NewCarDto carDto){
        ModelMapper modelMapper = new ModelMapper();
        Car car = modelMapper.map(carDto, Car.class);
        CarType carType = carTypeRepository.findOne(carDto.getCarType());
        Person person = personRepository.findOne(carDto.getOwner());
        car.setOwner(person);
        car.setCarType(carType);
        carRepository.save(car);
        return car;
    }


    private CarTypeDto getCarType(CarType carType){
        ModelMapper modelMapper = new ModelMapper();
        CarTypeDto carTypeDto = modelMapper.map(carType, CarTypeDto.class);
        return carTypeDto;
    }
}
