package pl.autokomis.Service;

import pl.autokomis.Dto.PersonDto;


public interface PersonService {

     void savePersonInfo(PersonDto personDto, Integer userId);

      PersonDto getPersonDetails(Integer id);

    void savePersonDetails(PersonDto personDto, String username);

}
