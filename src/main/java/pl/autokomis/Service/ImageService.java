package pl.autokomis.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.autokomis.Entity.Car;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class ImageService {

    private Logger log = LoggerFactory.getLogger(ImageService.class);

    public void saveImage(MultipartFile file, Car car){

        try {
            File image = new File("C:/Users/Dawid/Desktop/KURS_JAVA/AUTOKOMIS/autokomis/src/main/resources/static/images/"+car.getId()+".jpg");
            FileOutputStream fileOutputStream = new FileOutputStream(image);
            fileOutputStream.write(file.getBytes());
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            log.error(e.getMessage(),e);
            log.error("Błąd w zapisywaniu zdjęcia");
            throw new RuntimeException(e);
        } catch (IOException e) {
            log.error(e.getMessage(),e);
            log.error("Błąd w zapisywaniu zdjęcia");
            throw new RuntimeException(e);
        }
    }
}
