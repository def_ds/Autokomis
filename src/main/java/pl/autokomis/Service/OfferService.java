package pl.autokomis.Service;


import pl.autokomis.Dto.OfferCarDto;
import pl.autokomis.Entity.Agreement;

import java.util.List;

public interface OfferService {

     List<OfferCarDto> getCarListToSell();

     List<OfferCarDto> getCarArchList();

     Agreement soldCarById(Integer id);

     OfferCarDto getCarById(Integer id);
}
