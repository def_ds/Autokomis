package pl.autokomis.Service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.autokomis.Dto.OfferCarDto;
import pl.autokomis.Entity.Agreement;
import pl.autokomis.Entity.AgreementType;
import pl.autokomis.Entity.Car;
import pl.autokomis.Repository.AgreementRepository;
import pl.autokomis.Repository.AgreementTypeRepository;
import pl.autokomis.Repository.CarRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OfferServiceImpl implements OfferService{

    @Autowired
    private AgreementRepository agreementRepository;
    @Autowired
    private AgreementTypeRepository agreementTypeRepository;
    @Autowired
    private CarRepository carRepository;



    @Override
    public List<OfferCarDto> getCarListToSell(){
        AgreementType purchaseType = agreementTypeRepository.findOne(2);
        List<Agreement> agreements = agreementRepository.findAllByAgreementType(purchaseType);
        List<OfferCarDto> purchaseCar = agreements.stream().map(s -> agreementToSellOffer(s)).collect(Collectors.toList());

        AgreementType cessieonType = agreementTypeRepository.findOne(3);
        List<Agreement> agreements1 = agreementRepository.findAllByAgreementType(cessieonType);
        List<OfferCarDto> cessionCar = agreements1.stream().map(p-> agreementToSellOffer(p)).collect(Collectors.toList());

        List<OfferCarDto> offeredCars = new ArrayList<>(purchaseCar);
        offeredCars.addAll(cessionCar);
        return offeredCars;
    }

    @Override
    public List<OfferCarDto> getCarArchList() {
        AgreementType soldType = agreementTypeRepository.findOne(1);
        List<Agreement> agreements = agreementRepository.findAllByAgreementType(soldType);
        return agreements.stream().map(p-> agreementToSellOffer(p)).collect(Collectors.toList());
    }



    @Override
    public OfferCarDto getCarById(Integer id){
        Car car = carRepository.findOne(id);
        ModelMapper modelMapper = new ModelMapper();
        OfferCarDto carDetail = modelMapper.map(car, OfferCarDto.class);
        Agreement agreement = agreementRepository.findAgreementByIdCars(id);
        String agrementType = agreement.getAgreementType().getId().toString();
        carDetail.setAgreementType(agrementType);
        return carDetail;
    }



    @Override
    public Agreement soldCarById(Integer id) {
        Agreement agreement = agreementRepository.findAgreementByIdCars(id);
        agreement.setAgreementType(agreementTypeRepository.findOne(1));
        agreement.setAdded(new Date());
        agreement = agreementRepository.save(agreement);
        return agreement;
    }



    private OfferCarDto agreementToSellOffer(Agreement agreement){
        Car car = agreement.getCar();
        OfferCarDto offerCarDto = new OfferCarDto();
        offerCarDto.setId(car.getId());
        offerCarDto.setMark(car.getMark());
        offerCarDto.setModel(car.getModel());
        offerCarDto.setYear(car.getYear());
        offerCarDto.setDistance(car.getDistance());
        offerCarDto.setFuelType(car.getFuelType());
        offerCarDto.setDescription(car.getDescription());
        offerCarDto.setAmount(agreement.getAmount());
        offerCarDto.setAgreementType(agreement.getAgreementType().getId().toString());
        return offerCarDto;
    }

}
