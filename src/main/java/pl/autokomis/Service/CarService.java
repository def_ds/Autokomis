package pl.autokomis.Service;


import pl.autokomis.Dto.CarTypeDto;
import pl.autokomis.Dto.NewCarDto;
import pl.autokomis.Entity.Car;

import java.util.List;

public interface CarService {

    List<CarTypeDto> getCarTypeList();

    Car saveCar(NewCarDto carDto);
}
