package pl.autokomis.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import pl.autokomis.Dto.UserDto;
import pl.autokomis.Entity.Email;
import pl.autokomis.Repository.UserRepository;

import java.util.UUID;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private UserRepository userRepository;

    @Value("${server.port}")
    private String port;

    public void sendMail(UserDto userDto, Email type, String token){
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(userDto.getEmail());
        mail.setFrom("javaktw@gmail.com");
        mail.setSubject(type.getSub());
        mail.setText("Witaj " +userDto.getUsername()
                + ".Rejestracja przebiegła pomyslnie, proszę zatwierdzić naciskając link aktywacyjny: "
                + "http://localhost:"+port+"/"+ type.getUrlType()+"?token=" +token);
        javaMailSender.send(mail);
    }

    public String getToken(){
        return  UUID.randomUUID().toString();
    }
}
