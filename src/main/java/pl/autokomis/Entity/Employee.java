package pl.autokomis.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
//@Table(name = "employee")
@DiscriminatorValue("employee")
public class Employee extends Person{

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date hireDate;


    //Relations
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

}
