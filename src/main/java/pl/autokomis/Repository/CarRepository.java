package pl.autokomis.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.autokomis.Entity.Car;

@Repository
public interface CarRepository extends JpaRepository<Car,Integer> {
}
