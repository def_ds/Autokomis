package pl.autokomis.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.autokomis.Dto.NewCarDto;
import pl.autokomis.Dto.OfferCarDto;
import pl.autokomis.Entity.Agreement;
import pl.autokomis.Entity.Car;
import pl.autokomis.Service.*;


import java.util.List;

@Controller
public class CarController {

    @Autowired
    private OfferService offerService;

    @Autowired
    private CarService carService;

    @Autowired
    private UserService userService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private AgreementService agreementService;


    /**
     * agreement_type: 1)sales; 2)purchase; 3)cession;
     * */

    public final String CAR_LIST_TO_SELL = "CarList/carList";
    public final String CAR_DETAIL = "CarList/carDetail";
    public final String ADD_CAR = "CarForm/addCar";
    public static final String CAR_TYPE_ID = "carType";
    public static final String USER_ID = "users";
    public static final String AGREEMENT_TYPE = "agreement";
    /**
     * Method getCarListToSell - find all cars which are for sale.
     * @return car list for sale
     * */
    @GetMapping("/carsList")
    public String getCarListToSell(ModelMap modelMap){
        List<OfferCarDto> offerCarDtos = offerService.getCarListToSell();
        modelMap.addAttribute("carList", offerCarDtos);
        return CAR_LIST_TO_SELL;
    }


    /**
     * Method getCatDetails - show a detail a car by car id.
     * @return car details
     * */
    @GetMapping("/carsList/details")
    public String getCatDetails(@RequestParam("id") Integer id, ModelMap modelMap){
        if(id == null || id <= 0){
            throw new RuntimeException("Błąd identyfikatora pojazdu");
        }
        OfferCarDto car = offerService.getCarById(id);
        modelMap.addAttribute("carDetail", car);
        return CAR_DETAIL;
    }

    /**
     * Method soldCar - find agreement by car.id and changed a type on ('sold').
     * @param id - car id in database
     * */
    @GetMapping("/carsList/sold")
    public String soldCar(@RequestParam("id") Integer id, ModelMap modelMap){
        if(id == null || id <= 0){
            throw new RuntimeException("Błąd identyfikatora pojazdu");
        }
        Agreement agreement = offerService.soldCarById(id);
        return "redirect:/soldCarList";
    }

    /**
     * Method getCarListToSell - find all cars which were sold.
     * @return car sold list
     * */
    @GetMapping("/soldCarList")
    public String getCarArchList(ModelMap modelMap){
        List<OfferCarDto> offerCarDtos = offerService.getCarArchList();
        modelMap.addAttribute("carList", offerCarDtos);
        return CAR_LIST_TO_SELL;
    }


    /**
     * Method addCar - form for add car.
     * @return form to add car
     * */
    @GetMapping("/addCar")
    public String addCar(ModelMap modelMap){
        modelMap.put("carForm", new NewCarDto());
        modelMap.addAttribute(CAR_TYPE_ID, carService.getCarTypeList());
        modelMap.addAttribute(USER_ID, userService.getPersonList());
        modelMap.addAttribute(AGREEMENT_TYPE, agreementService.getAgreementTypeList());
        return ADD_CAR;
    }


    /**
     * Method saveCar - add car for salesale.
     * @return car list for sale
     * */
    @RequestMapping("/addCar")
    public String saveCar(@RequestPart(value ="image", required = false) MultipartFile file,
                          @ModelAttribute("carForm") NewCarDto carDto, ModelMap modelMap){
        Car car = carService.saveCar(carDto);
        agreementService.saveAgreement(carDto, car.getId());
        imageService.saveImage(file, car);
        return "redirect:/carsList";
    }
}
