package pl.autokomis.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AccessDenied {

    @GetMapping("/access-denied")
    public String showDeniedPage(ModelMap modelMap){
        return "access-denied";
    }
}
